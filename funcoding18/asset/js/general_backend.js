function onClick(){
	var code = document.getElementById("code").value;
	localStorage.setItem('participantCode', code);
	localStorage.setItem('eval', true);
	location.reload();
}

function resetCode() {
	$("#code").val('');
}

function closeModal(level){
	$("#introModal").hide()
	localStorage.setItem(level, true);
}

function showModal(){
	$("outroModal").show()
}

function hideOutroModal(){
	$("#outroModal").hide()
}

$( document ).ready(function() {
	if (localStorage.getItem('participantCode') !== null) {
		if (localStorage.getItem('eval') == "true" ) {
			var code = localStorage.getItem('participantCode')
			$("#code").val(code);
			localStorage.setItem('eval', false);
			localStorage.removeItem('participantCode');
			setTimeout(function(){ eval(code); }, 500);
		} else {
			var code = localStorage.getItem('participantCode')
			$("#code").val(code);
			localStorage.removeItem('participantCode');
		}
	}
});