localStorage.clear()

function showBiodata() {
	$('#mulaiButton').hide();
	$('.welcomeRow').css("margin-top","15vh");
	$('#formBiodata').fadeIn();
}

function gender(gend) {
	if(gend == 'l' && !($('#butMale').hasClass("button-primary"))) {
		$('#butMale').addClass("button-primary");
		$('#butFemale').removeClass("button-primary");
		$('#butMale').html("&#10004; Laki-laki");
		$('#butFemale').html("Perempuan");
	} else if (gend == 'f' && !($('#butFemale').hasClass("button-primary"))) {
		$('#butFemale').addClass("button-primary");
		$('#butMale').removeClass("button-primary");
		$('#butMale').html("Laki-laki");
		$('#butFemale').html("&#10004; Perempuan");
	}
}