// need util.js

function saveData() {
	if($("#participantName").val() == '') {
		$('#errMsg').fadeIn(500);
		$('#errMsg').delay(1000).fadeOut(1000);
	} else {
		parName = $("#participantName").val();
		if($('#butMale').hasClass("button-primary")) {
			gd = 'l';
		} else {
			gd = 'f';
		}
		
		var obj = {name:parName, gender: gd};
		localStorage.setItem('participantData', JSON.stringify(obj));
		
		// Change the location to the first level
		window.location = "level1.html";
	}
}

function showData() {
	if (localStorage.getItem('participantData') === null) {
		//Belum masukin data, show modal untuk balikin mereka ke landing Page
		console.log("kosong");
		$("#myModal").show();
	} else {
		var obj = JSON.parse(localStorage.getItem('participantData'));
		$("#participantName").html(obj.name);
		
		if(obj.gender == 'l') {
			$("#participantGender").html("saudara");
		} else {
			$("#participantGender").html("saudari");
		}
	}
}

$( document ).ready(function() {
	// Show data only on game page
	if(getCurrentFileName() != "index.html") {
		showData();
		console.log(getCurrentFileName())
	}
});

function getCurrentFileName(){
    var pagePathName= window.location.pathname;
    return pagePathName.substring(pagePathName.lastIndexOf("/") + 1);
}